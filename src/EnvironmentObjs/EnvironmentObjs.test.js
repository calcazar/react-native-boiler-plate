import EnvironmentObjs, {EnvironmentObject} from './EnvironmentObjs.js';
//__envObjs__ = {test: 1}
describe("The Envrionment Object file", () => {

	it("should return a blank object if the Environment variable is undefined",()=>{
		expect(EnvironmentObjs).toEqual({})
	} )

	it("should return the overriden object if an override is passed",()=>{
		let EnvObj = new EnvironmentObject({fakeData: "fakeData"});
		expect(EnvObj.getEnvironmentObjects()).toEqual({fakeData: "fakeData"})
	} )
})