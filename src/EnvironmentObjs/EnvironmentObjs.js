export class EnvironmentObject{
	constructor(override) {
		let envObj;
		try {
			envObj = __envObjs__;
		}
		catch (e){
			envObj = {};
		}
		
		if (override !== undefined) {
			Object.assign(envObj, override)
		}
		this.envObj = envObj;


	}
	getEnvironmentObjects() {
		return this.envObj;
	}
}
let EnvObj = new EnvironmentObject();
export default EnvObj.getEnvironmentObjects()