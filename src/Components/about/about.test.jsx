import React from 'react';
import {mount} from 'enzyme'; //used to mount the shadow dom
import sinon from 'sinon'; //used to test against component prototypes
import About from './about.jsx';

describe('<About/>', () => {
	it('should call someRandomFunction', () => {
		sinon.spy(About.prototype, 'someRandomFunction');
		const wrapper = mount(<About />);
		wrapper.find('p').simulate('click');
		expect(About.prototype.someRandomFunction.calledOnce).toEqual(true);
	});
	it('should render a p tag with greeting text', () => {
		const wrapper = mount(<About />);
		expect(wrapper.contains(<p onClick={wrapper.instance().someRandomFunction}>This is the super amazing About page!</p>)).toEqual(true);
	});
});