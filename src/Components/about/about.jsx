import React from 'react';

import './about.scss';

export default class About extends React.Component {
	constructor() {
		super();
		this.someRandomFunction = this.someRandomFunction.bind(this);
	}
	someRandomFunction() {
		this.setState({
			"test":"woo!"
		});
	}
	render() {
		return (
			<div className="aboutPage">
			<p onClick={this.someRandomFunction}>This is the super amazing About page!</p>
			</div>
		)
	}
}