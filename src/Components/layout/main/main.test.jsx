import React from 'react';
import {mount} from 'enzyme'; //used to mount the shadow dom
import Main from './Main.jsx';

import Nav from './nav/nav.jsx';
import Footer from './footer/footer.jsx';
import PageRoutes from './pageRoutes/pageRoutes';

describe('<Main/>', () => {
	it('should render the <Nav /> component', () => {
		const wrapper = mount(<Main />);
		expect(wrapper.contains(<Nav />)).toEqual(true);
	});
	it('should render the <Footer /> component', () => {
		const wrapper = mount(<Main />);
		expect(wrapper.contains(<Footer />)).toEqual(true);
	});
	it('should render the <PageRoutes /> component', () => {
		const wrapper = mount(<Main />);
		expect(wrapper.contains(<PageRoutes />)).toEqual(true);
	});

});