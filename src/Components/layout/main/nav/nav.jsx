import React from 'react';
import {NavLink} from 'react-router-dom';
import {FaHome} from 'react-icons/lib/fa';

import './nav.scss'
export default class Nav extends React.Component {
	constructor() {
		super();
		this.renderLinks = this.renderLinks.bind(this);
	}
	renderLinks() {
		const links = [
			{ url:"/", text:"Home" },
			{ url:"/about", text:"About" },
			{ url:"/contact", text:"Contact" }
		],
		renderedLinks = links.map((link,index) => {
			return <NavLink activeClassName="active" key={index} exact to={link.url}>{link.text} </NavLink>
		});
		return renderedLinks;
	}
	render() {
		return (
			<nav className="mainNav">
				{this.renderLinks()}
			</nav>
		)
	}
}

