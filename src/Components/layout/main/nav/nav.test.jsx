import React from 'react';
import {mount} from 'enzyme'; //used to mount the shadow dom
import Nav from './nav';
import {BrowserRouter as Router,Link} from 'react-router-dom';

describe('<Contact/>', () => {
	let wrapper;
	beforeEach(() => {
		wrapper = mount(<Router><Nav /></Router>);
	});
	it('Should contain 3 links', () => {
		expect(wrapper.find('nav Link')).toHaveLength(3);
	});
	it('First Link Should be the home link', () => {
		expect(wrapper.find('nav Link').nodes[0].props.children).toEqual("Home");
		expect(wrapper.find('nav Link').nodes[0].props.to).toEqual("/");
	});
	it('Second Link Should be the about link', () => {
		expect(wrapper.find('nav Link').nodes[1].props.children).toEqual("About");
		expect(wrapper.find('nav Link').nodes[1].props.to).toEqual("/about");
	});
	it('Third Link Should be the contact link', () => {
		expect(wrapper.find('nav Link').nodes[2].props.children).toEqual("Contact");
		expect(wrapper.find('nav Link').nodes[2].props.to).toEqual("/contact");
	});
});