import React from 'react';
import {mount} from 'enzyme'; //used to mount the shadow dom
import Footer from './footer';

describe('<Foter/>', () => {
	it('should render a p tag with Footer text', () => {
		const wrapper = mount(<Footer />);
		expect(wrapper.contains(<p>I am a footer</p>)).toEqual(true);
	});
});