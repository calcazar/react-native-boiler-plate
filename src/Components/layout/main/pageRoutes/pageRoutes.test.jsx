import React from 'react';
import {shallow} from 'enzyme'; //used to mount the shadow dom
import { Route } from 'react-router';
import PageRoutes from './PageRoutes.jsx';
import Dashboard from '../../../dashboard/dashboard.jsx';
import Contact from '../../../contact/Contact.jsx';
import About from '../../../about/about.jsx';

describe('<About/>', () => {
	describe('Page Routes should redirect to the correct pages', () => {
		let pathMap
		let wrapper
		beforeAll(() => {
			wrapper = shallow(<PageRoutes />);
			pathMap = wrapper.find(Route).reduce((pathMap, route) => {
			    const routeProps = route.props();
			    pathMap[routeProps.path] = routeProps.component;
			    return pathMap;
  			}, {});
		})
		it('should render the <About /> component', () => {
			expect(pathMap['/about']).toBe(About);
		});
		it('should render the <Dashboard /> component', () => {
			expect(pathMap['/']).toBe(Dashboard);
		});
		it('should render the <Contact /> component', () => {
			expect(pathMap['/contact']).toBe(Contact);
		});
	})
});