import React from 'react';
import Dashboard from '../../../dashboard/dashboard.jsx';
import Contact from '../../../contact/Contact.jsx';
import About from '../../../about/about.jsx';
import {Route} from 'react-router-dom';

export default routes => (
		<div>
			<Route exact path="/" component={Dashboard} />
			<Route path="/about" component={About} />
			<Route path="/contact" component={Contact} />
		</div>
)