import actionTypes from "../../../redux/ActionTypes.js"
import axios from 'axios';

export function changeGreetingText(message, isTourActive) {
	if (isTourActive) {
		console.log("We are now in the Action!");
		console.log(`Actions don't communicate with the store directly. Infact they communicate with the
			reducers. When our component dispatches an action, it is expecting the action to return the 
			mandatory property of type (lowercase) and any other properties you want to make up. The dispatch
			function then reads the type and broadcasts to the reducers that this specific type has just been 
			called.
		`);
		debugger
	}
	return {
		type: actionTypes.CHANGE_GREETING_TEXT,
		payload: {
			message: message,
			isTourActive: isTourActive
		}

	}
}

export function getAPIData() {
	let url = "http://localhost:8081/posts";
	return function(dispatch) {
		dispatch({type: actionTypes.PREPARING_TO_GET_DATA});
		setTimeout(function(){ //Set timeout added to simulate data fetching times
			axios.get(url).then((response) => {
				dispatch({type: actionTypes.RECEIVED_DATA, payload: response.data});
			});
		}, 2000);
	}
}