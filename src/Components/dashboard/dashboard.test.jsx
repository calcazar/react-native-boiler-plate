import React from 'react';
import {mount, shallow} from 'enzyme'; //used to mount the shadow dom
import sinon from 'sinon'; //used to test against component prototypes
import Dashboard from './dashboard.jsx';
import { Provider } from 'react-redux';
import store from '../../redux/store.js';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

describe('<Dashboard/>', () => {
	it("should render the default greeting on load ", () => {
		const wrapper = mount(<Provider store={store}><Dashboard /></Provider>);
		expect(wrapper.contains(<p>Hello</p>)).toEqual(true);
	})
	describe('The button:', () => {
		it("reduxWermHoleBtn should render a redux tour button with the correct Text ", () => {
			const wrapper = mount(<Dashboard.WrappedComponent />);
			expect(wrapper.find("#reduxWermHoleBtn").text()).toEqual("Let's go through the REDUX WERM HOLE TOUR!");
		})
		it("reduxWermHoleBtn should call the reduxWermHole function onClick ", () => {
			sinon.spy(Dashboard.WrappedComponent.prototype, 'reduxWermHole');
			const wrapper = mount(<Provider store={store}><Dashboard /></Provider>);
			wrapper.find("#reduxWermHoleBtn").simulate("click");
			expect(Dashboard.WrappedComponent.prototype.reduxWermHole.calledOnce).toEqual(true);
		})
		it("ajaxCallBtn should render a redux tour button with the correct Text ", () => {
			const wrapper = mount(<Dashboard.WrappedComponent />);
			expect(wrapper.find("#ajaxCallBtn").text()).toEqual("Let's make an AJAX Call!!");
		})
		it("ajaxCallBtn should call the ajaxCall function onClick ", () => {
			sinon.spy(Dashboard.WrappedComponent.prototype, 'ajaxCall');
			const wrapper = mount(<Provider store={store}><Dashboard /></Provider>);
			wrapper.find("#ajaxCallBtn").simulate("click");
			expect(Dashboard.WrappedComponent.prototype.ajaxCall.calledOnce).toEqual(true);
		})
		it("ajaxCallBtn when clicked should show fetching data text ", () => {
			const wrapper = mount(<Provider store={store}><Dashboard /></Provider>);
			wrapper.find("#ajaxCallBtn").simulate("click");
			expect(wrapper.contains(<div>I am Fetching Ze Data!</div>)).toEqual(true);
		})
		it("ajaxCallBtn when clicked shouldfetch data and display the data that was fetched ", done => {
			var mock = new MockAdapter(axios);
			mock.onGet('http://localhost:8081/posts').reply(200, [
					{
						title: "OMG!"
					}
				]
			)
			jest.useRealTimers();
			const wrapper = mount(<Provider store={store}><Dashboard /></Provider>);
			wrapper.find("#ajaxCallBtn").simulate("click");
			setTimeout(() => {	
				try {
					expect(wrapper.contains(<h1>Fetched Data:</h1>)).toEqual(true);
					expect(wrapper.contains(<b>OMG!</b>)).toEqual(true);
				} catch (ex){
					done.fail(ex);	
				} finally {
					done();
				}	
			}, 2100)
			
				
		})
	})
});