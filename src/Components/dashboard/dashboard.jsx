import React from 'react';
import {connect} from 'react-redux';
import {changeGreetingText, getAPIData} from "./actions/actions.js";
import "./dashboard.scss";

class SectionContainer extends React.Component{
	constructor() {
		super();
	}
	render(){
		return(
			<div className={`${this.props.className} sectionContainer`}>
				<header>
					{this.props.header}
				</header>
				<section>
					{this.props.children}
				</section>
			</div>
		)
	}
}

@connect((store) => {
	if(store.dashboardReducer.isTourActive) {
		console.log(`
			The reducer updated the Store and we are back here in the component!!. Once the store
			updated, it triggered a change of state in our component which is now going to rerendered
			it.
		`);
		debugger;
	}

	return {
		messages: store.dashboardReducer.message,
		isFetchingData: store.dashboardReducer.isFetchingData,
		data: store.dashboardReducer.data
	};
})
export default class dashboard extends React.Component {
	constructor(props) {
		super(props);
		this.reduxWermHole = this.reduxWermHole.bind(this);
		this.ajaxCall = this.ajaxCall.bind(this);
	}
	reduxWermHole() {
		console.log("Click Handler calls the action");
		console.log("Our Actions house our Ajax calls or submit any data changes that need to be rerendered");
		debugger;
		this.props.dispatch(changeGreetingText("FIRST REDUX APP SUCCESS!!",true));
	}
	ajaxCall() {
		this.props.dispatch(getAPIData());
	}
	render() {
		return (
			<div className="dashboardPage">
			<h1>Dashboard</h1>
				<SectionContainer header="Redux Message" className="messageBox">
					<p>{this.props.messages}</p>
					<button id="reduxWermHoleBtn" onClick={this.reduxWermHole}>Let's go through the REDUX WERM HOLE TOUR!</button>
				</SectionContainer>
				
				<SectionContainer header="Ajax Call" className="ajaxBox">
				{this.props.isFetchingData &&
					<div><img className="spinner small" src="/assets/img/ring-alt.gif"/></div>
				}
				{(!this.props.isFetchingData && this.props.data) && 
					<div>
					<h2>Fetched Data:</h2>
					<p className="response">{this.props.data[0].title}</p>
					</div>
				}
					<button id="ajaxCallBtn" onClick={this.ajaxCall}>Let's make an AJAX Call!!</button>
				</SectionContainer>
				

				

			</div>
		)
	}
}