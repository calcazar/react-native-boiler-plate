const indexTypes = {
	CHANGE_GREETING_TEXT: "CHANGE_GREETING_TEXT",
	PREPARING_TO_GET_DATA: "PREPARING_TO_GET_DATA",
	RECEIVED_DATA: "RECEIVED_DATA"
};

export default indexTypes