import actionTypes from "../../../redux/ActionTypes.js"

const initalState = {
	message: "Hello",
	isTourActive: false
}

export default function dashboardReducer(state = initalState, action) {
	switch(action.type){
		case actionTypes.CHANGE_GREETING_TEXT:
			if (action.payload.isTourActive) {
				console.log(`We are now in the reducer!!! ALL reducers get called which is why ALL reducers have logic
					surrounding which action type to listen for. In this case the CHANGE_GREETING_TEXT action type was 
					called which lands in our switch statement case and returns an immutable copy of the components 
					state and our modification to the message property.`);
				console.log(`
					Mind you, this state change goes to the store which is managed by Redux. Once the store is changed
					a rerender will occur on the component.
				`);
				debugger;
			}
			return {...state, message: action.payload.message, isTourActive: action.payload.isTourActive}
		break;
		case actionTypes.PREPARING_TO_GET_DATA:
			return {...state, isFetchingData: true}
		break;
		case actionTypes.RECEIVED_DATA:
			return {...state, isFetchingData: false, data: action.payload}
		break;
	}
	return state;

}