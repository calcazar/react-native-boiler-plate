const contactTypes = {
	CHANGE_CONTACT_PAGE: "CHANGE_CONTACT_PAGE",
	POST_DATA: "POST_DATA"
};

export default contactTypes