import React from 'react';
import {mount} from 'enzyme'; //used to mount the shadow dom
import Contact from './Contact';
import store from '../../redux/store.js';
import { Provider } from 'react-redux';
import sinon from 'sinon';
import axios from 'axios';

describe('<Contact/>', () => {
	it('should render a p tag with greeting text', () => {
		const wrapper = mount(<Provider store={store}><Contact /></Provider>);
		expect(wrapper.contains(<p>This is the contact page</p>)).toEqual(true);
	})
	it("should call buttonClick function", () => {
		sinon.spy(Contact.WrappedComponent.prototype, "buttonClick");
		const wrapper = mount(<Provider store={store}><Contact /></Provider>);
		wrapper.find("button").simulate("click");
		expect(Contact.WrappedComponent.prototype.buttonClick.calledOnce).toEqual(true);
	});
	it("should change button text when button is clicked", () => {
		const wrapper = mount(<Provider store={store}><Contact /></Provider>);
		wrapper.find("button").simulate("click");
		expect(wrapper.find("button").text()).toEqual("Contact Us Now!!");
	});
	it("form should render a redux form with the correct input and label ", () => {
		const wrapper = mount(<Contact.WrappedComponent />);
		expect(wrapper.find("label").text()).toEqual("Enter Your First Name:");
	});
	it("form submit should call the ajaxCall2 function onSubmit ", () => {
		sinon.spy(Contact.WrappedComponent.prototype, 'ajaxCall2');
		const wrapper = mount(<Provider store={store}><Contact /></Provider>);
		wrapper.find("form").simulate("submit");
		expect(Contact.WrappedComponent.prototype.ajaxCall2.calledOnce).toEqual(true);
	});
})