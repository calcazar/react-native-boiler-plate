import React from 'react';
import {changeContactThing, postAPIData} from "./actions/actions.js";
import {connect} from 'react-redux';

@connect((store) => {
debugger;
console.log(store);
	return {
		messages: store.contactReducer.message,
		isPostingData: store.contactReducer.isPostingData,
		data: store.contactReducer.data
	};
})
export default class Contact extends React.Component {
	constructor(props) {
		super(props);
		this.buttonClick = this.buttonClick.bind(this);
		this.ajaxCall2 = this.ajaxCall2.bind(this);
		this.handleFirstName = this.handleFirstName.bind(this);

		this.state = {firstName: ''};
	}
	buttonClick() {
		console.log("clicked");
		this.props.dispatch(changeContactThing("Contact Us Now!!", true));
	}
	handleFirstName(e) {
		this.setState({firstName: e.target.value});
	}
	ajaxCall2(e) {
		e.preventDefault();
		this.props.dispatch(postAPIData(this.state.firstName));
	}

	render() {
		return(
			<div>
				<p>This is the contact page</p>
				<button onClick={this.buttonClick}>{this.props.messages}</button>

				<form onSubmit={this.ajaxCall2}>
			        <label>
			          Enter Your First Name:
			          <input type="text" onChange={this.handleFirstName} />
			        </label>
			        <input id="submit" type="submit" value="Submit" />
		      	</form>

				{(this.props.data) && 
					<div>
					<h1>This is your First Name:</h1>
					<br />
					<b>{this.props.data.firstName}</b>
					</div>
				}
			</div>
		)
	}
}