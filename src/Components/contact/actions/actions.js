import actionTypes from "../../../redux/ActionTypes.js"
import axios from 'axios';

export function changeContactThing(message, isTrue) {

		console.log("We are now in the Contact Action!");
		console.log(message);
		
	return {
		type: actionTypes.CHANGE_CONTACT_PAGE,
		payload: {
			message: message,
			isTrue: isTrue
		}
	}
}

export function postAPIData(firstName) {
	let url = "https://jsonplaceholder.typicode.com/posts";

	return function(dispatch) {
		axios.post(url, firstName: firstName).then((response) => {
			
			dispatch({type: actionTypes.POST_DATA, payload: {firstName:firstName}});
			
		});
	}
}
