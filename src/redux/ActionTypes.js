import dashboardTypes from "../Components/dashboard/actionTypes/actionTypes.js";
import contactTypes from "../Components/contact/actionTypes/actionTypes.js";

import objExtend from "extend";

export class combinedTypesClass {
	constructor(typesList) {
		this.seen = new Set();
		this.duplicateTypeCheck = this.duplicateTypeCheck.bind(this);
	}


	duplicateTypeCheck(currentObject) {
		return this.seen.size === this.seen.add(Object.keys(currentObject)[0]).size;
	}
	convertTypesListToObject(combinedTypes) {
		var typesObj = {};
		combinedTypes.map((type)=>{
			objExtend(true, typesObj, type);
		});
		return typesObj;
	}
	combinedTypes(combinedTypes) {
		var hasDuplicates = combinedTypes.some(this.duplicateTypeCheck);
		if (hasDuplicates) {
			throw new Error("Duplicate Type error - ActionTypes/index.jsx: You have a type that is conflicting with another predefined type. If you added a new Type just now, rename it or reuse the existing type in your reducer!");
		}
		return this.convertTypesListToObject(combinedTypes);
	}
}

export function getCombinedTypes(combinedTypes) {
	let combinedTypesObj = new combinedTypesClass();

	return combinedTypesObj.combinedTypes(combinedTypes);
}







export default getCombinedTypes([
		dashboardTypes,
		contactTypes
	]);