import { combineReducers } from "redux";

import dashboardReducer from "../Components/dashboard/reducer/reducer.js";
import contactReducer from "../Components/contact/reducer/reducer.js";

export default combineReducers({
	dashboardReducer,
	contactReducer
})