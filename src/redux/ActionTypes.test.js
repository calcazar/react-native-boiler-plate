import {combinedTypesClass, getCombinedTypes} from './ActionTypes.js';
import dashboardTypes from "../Components/dashboard/actionTypes/actionTypes.js";

import sinon from 'sinon';

describe('actionTypeIndex', () => {
	describe('Contains a combinedTypesClass', () => {
		describe("with a combinedTypes function", () => {
			it("That gets called on instantiation", () => {
				sinon.spy(combinedTypesClass.prototype, 'combinedTypes');
				getCombinedTypes([
					dashboardTypes
				]);
				expect(combinedTypesClass.prototype.combinedTypes.calledOnce).toEqual(true);
			})
			
		})
			it("returns an error when duplicate types are present", () => {
				function testWrapper() {
					getCombinedTypes([
						dashboardTypes,
						dashboardTypes
					])
				}
				expect(testWrapper).toThrowError(/Duplicate Type error/);
			})
			it("returns an object when no duplicates are present", () => {
				const result = getCombinedTypes([
						dashboardTypes
					])
				

				expect(typeof result).toBe("object");
			})
		//const wrapper = mount(<Contact />);
		//expect(wrapper.contains(<p>This is the contact page</p>)).toEqual(true);
	});
});