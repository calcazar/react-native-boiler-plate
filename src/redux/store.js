import {applyMiddleware, createStore} from "redux";

import logger from 'redux-logger'
import thunk from "redux-thunk";
import promise from "redux-promise";

import reducer from './reducer.js';
import envObjs from '../EnvironmentObjs/EnvironmentObjs.js';

function getTestingMiddleWare() {
	console.log = function(){};
	return applyMiddleware(promise, thunk);
}

function getDefaultMiddleWare() {
	return applyMiddleware(promise, thunk);
}
function getDevMiddleWare() {
	return applyMiddleware(logger, promise, thunk);
}

function getMiddleWare() {
	if (process.env.TESTING === "true") {
		return getTestingMiddleWare();
	}

	if (envObjs.environment === "Dev") {
		return getDevMiddleWare();
	}

	return getDefaultMiddleWare();
}

export default createStore(reducer, getMiddleWare());