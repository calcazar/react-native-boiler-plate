import React from 'react';
import {render} from 'react-dom';
import AppContainer from './Components/layout/main/main.jsx';

import { Provider } from 'react-redux';
import store from './redux/store.js';
import "./app.scss";

render(
	<Provider store={store}>
		<AppContainer/>
	</Provider>, 
	document.getElementById('app'));