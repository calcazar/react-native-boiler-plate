import axios from 'axios';

export function setTranslationFile(language) {

	let url = `assets/locale/${language}.json`;

	return function(dispatch) {
		axios.get(url)
			.then((response) => {
				dispatch({type: "FETCHING_LANGUAGE_DONE", locale: language, phrases: response.data});
			})
	}
}
