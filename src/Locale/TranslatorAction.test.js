import {setTranslationFile} from './TranslatorAction.js';
import axios from 'axios';
import mockAdapter from'axios-mock-adapter';
import sinon from 'sinon';



describe("The Translator Action",()=>{
	it("contains a setTranslationFile function", ()=>{
		expect(setTranslationFile).toBeDefined()
	})
	it("should make an Ajax Call to get a language file", ()=>{
		let spy = sinon.spy(axios, 'get');
		const getCallBack = setTranslationFile("en");
		getCallBack();
		expect(axios.get.calledOnce).toEqual(true);
		spy.restore();
	})

	it("Should Dispatch FETCHING_LANGUAGE_DONE action on success",(done)=>{
		function fakeDispatch(Dispatch) {
			try {
				expect(Dispatch.type).toEqual("FETCHING_LANGUAGE_DONE");
			} catch (ex){
				done.fail(ex);	
			} finally {
				done();
			}	
			
		}

		let mock = new mockAdapter(axios);
		mock.onGet('assets/locale/en.json').reply(200, {
			mockPhrases: "Mock Phrases"
		});
		const getCallBack = setTranslationFile("en");
		getCallBack(fakeDispatch);
	})
} )