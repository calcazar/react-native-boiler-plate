var webpack = require('webpack');
var CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var babelJest = require('babel-jest');
var BUILD_DIR = null;

if (process.env.BUILD=== "all") {
	BUILD_DIR = path.resolve(__dirname, `dist/packaging/${process.env.NODE_ENV}`);
} else {
	BUILD_DIR = path.resolve(__dirname, 'dist/');
}

var APP_DIR = path.resolve(__dirname, 'src/');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin')
var envObjs;
var setupEnvironmentConfig = function() {
	switch(process.env.NODE_ENV) {
		case "dev":
			envObjs = JSON.stringify(require(`${APP_DIR}/EnvironmentObjs/JSON/dev.json`));
		break;
		case "int":
			envObjs = JSON.stringify(require(`${APP_DIR}/EnvironmentObjs/JSON/int.json`));
		break;
		case "cert":
			envObjs = JSON.stringify(require(`${APP_DIR}/EnvironmentObjs/JSON/cert.json`));
		break;
		default:
			envObjs = JSON.stringify(require(`${APP_DIR}/EnvironmentObjs/JSON/prod.json`));
		break;
	}
	
}
setupEnvironmentConfig();

var config = {
	entry: ['babel-polyfill', 'react', 'react-dom', APP_DIR + '/app.jsx'],
	devServer: {
		historyApiFallback: true
	},
	output: {
		path: BUILD_DIR,
		filename: 'assets/bundle-[hash:6].js'
	},
	module : {
		rules: [
		{
			test: /\.scss$/, 
			use: ExtractTextPlugin.extract({
				fallback: 'style-loader',
				use: ['css-loader', 'sass-loader' ]
			})
		},

		{
			test : /\.jsx?/,
			include : APP_DIR,
			loader : 'babel-loader'
		},
		{ test: /\.png$/, loader: 'file-loader' },
		{ test: /\.ico$/, loader: 'file-loader' },

        { test: /\.svg$/, loader: 'url?limit=65000&mimetype=image/svg+xml&name=public/fonts/[name].[ext]' },
        { test: /\.woff$/, loader: 'url?limit=65000&mimetype=application/font-woff&name=public/fonts/[name].[ext]' },
        { test: /\.woff2$/, loader: 'url?limit=65000&mimetype=application/font-woff2&name=public/fonts/[name].[ext]' },
        { test: /\.[ot]tf$/, loader: 'url?limit=65000&mimetype=application/octet-stream&name=public/fonts/[name].[ext]' },
        { test: /\.eot$/, loader: 'url?limit=65000&mimetype=application/vnd.ms-fontobject&name=public/fonts/[name].[ext]' },
    	{ test: /\.json$/, loader: 'json-loader'}]
	},
	plugins: [
			new CaseSensitivePathsPlugin(),
			new webpack.DefinePlugin({
				"__envObjs__": envObjs
			}),
			new HtmlWebpackPlugin({
				template: `${APP_DIR}/templates/index.template.ejs`,
				inject: 'body',
				title: "Boiler Plate",
			}),
			new OptimizeCssAssetsPlugin(),
			new CopyWebpackPlugin(requiredFiles()),
			new CleanWebpackPlugin(cleanDistFolder(),{"verbose": false}),
			new ExtractTextPlugin('assets/style-[hash:6].css'),

	]
};
function cleanDistFolder(){
	if (process.env.BUILD === "all" && process.env.NODE_ENV === "dev") {
		return ["dist"]
	}
	if (process.env.BUILD === "all") {
		return ["dist/packaging/" + process.env.NODE_ENV]
	} else {
		return ["dist"]
	}
}
function requiredFiles() {
	var initialFiles =[
		{context: `${APP_DIR}/Locale`, from: '*.json', to: 'assets/locale' },
		{from: `${APP_DIR}/assets`, to:'assets'}
	];
	if (process.env.BUILD=== "all" && process.env.NODE_ENV ==="prod") {
		initialFiles.push({from: `${APP_DIR}/BuildFiles`, to:'../BuildFiles'});
		initialFiles.push({from: `${APP_DIR}/Doc`, to:'../Doc'});
	}
	return initialFiles;
}
module.exports = config;
