#### Technologies in this boiler plate are:

* Webpack
	* Webpack Dev Server
	* HTML Webpack Plugin
* React
	* React Router
* Redux
	* Redux Logger
	* Redux Thunk
	* Redux Promise
* Jest
	* Sinon
	* Enzyme
* Sass + Loaders
* React Foundation
* Extend
* Babel
* Axios

#### This boiler plates also includes examples of Ajax calls and has a Redux tour setup with a handful of debuggers to help you navigate the code.

## Terminal Commands:
* npm run dev - runs build, launches server, initiates watch
* npm test - runs tests
* npm run build - builds project in to the release folder as a release candidate
